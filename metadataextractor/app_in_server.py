import sys
from flask import Flask
from flask import render_template
from flask import request
app = Flask(__name__)

sys.path.append("/home/wishva/mysite/RAKE-tutorial")
import rake
import operator
rake_object = rake.Rake("/home/wishva/mysite/RAKE-tutorial/SmartStoplist.txt", 4, 1, 2)

from nltk.stem.wordnet import WordNetLemmatizer
from nltk.stem import porter
import ast
from nltk.corpus import wordnet
#wordnet.synsets('biolog')

lemmy = WordNetLemmatizer()
stemmer = porter.PorterStemmer()

def correct_tag(tags):
    """
    Takes in one tag and 'corrects' it by lemmatizing it and removing entries with single values

    Parameters
    ----------
    tag: a list of tags

    """
    print tags
    if type(tags) == str:
        tags = ast.literal_eval(tags)

    #break the tag if it has more than one word
    lemmy_list = []

    for single_tag in tags:
        if '>' in single_tag:
            single_tag.replace('>',' ')
        tagz = single_tag.strip().split()

        #lemmatizing
        for t in tagz:
            t = t.strip()
            if len(t) > 1:
                try:
                    lemmy_list.append((lemmy.lemmatize(t)))
                except:
                    continue #fix this

    #stemming
    stem_list = []
    for l in lemmy_list:
        try:
            print l
            stemmed_word = stemmer.stem(l)

            if wordnet.synsets(stemmed_word) == []:
                #not in wordnet...revert to stemmer
                stem_list.append(l)
            else:
                stem_list.append(stemmer.stem(l))
        except:
            continue
            r = list(set(stem_list))
            print r,'ss'
            if len(r) == 0:
            r.append(" ")

    return list(set(stem_list))


@app.route('/')
def index():
    return render_template("index.html")



@app.route('/extractTags/', methods=['GET','POST'])
def extractTags():

    #load exclude word list
    exclude_word_list = []
    for line in open('/home/wishva/mysite/data/common_words.txt','r'):
    exclude_word_list.append(line.strip())



    output_string = ""

    search_string = request.form['description']

    #remove punctuationmarks from string
    puncs = """'!"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~'"""
    for p in puncs:
    search_string = search_string.replace(p," ")


    rake_output = rake_object.run(search_string)

    #adding RAKE data to the output_string
    output_string = """@@ Tags identified by Rapid Automatic Keyword Extraction (RAKE) algorithm @@""" + "\n"
    #output_string = output_string +  """______________________________________________________________________""" + "\n"
    rake_list = []
    for word,_ in rake_output:
    rake_list.append(word)
    rake_list = set(rake_list)


    for rw in rake_list:
    rw = str(rw)
output_string = output_string + str(rw) + " -->" + "\n"



#finding single word tags with at least one capital letter
upper_case_list = []
for word in search_string.strip().split():
word = str(word)
                if word.istitle():
                    continue
                ww = word.lower()
            if ww in exclude_word_list:
        continue
    if word == word.lower():
#then its simple
pass
else:+ custom_text
#processing custom_text
return_list = []
for ct in big_text.strip().split('\n'):
print "now working on", ct
if ct == "":
    continue
ct1 = ct.strip().split('-->')
print ct1
if ct1[0].strip() == "":
    #line is empty
    continue
if ct1[1].strip() == "":
    #no new value specified
    #if they are in my_text (acronyms) just add to the return_list
    if ct1[0].strip() in my_text:
    return_list.append(ct1[0].strip())
        continue
print 'zzz',correct_tag([ct1[0].strip()])
if len(ct1[0]) <= 2:
continue
return_list.append(str(correct_tag([ct1[0].strip()])[0]))
elif ct1[1].strip() == 'x' or ct1[1].strip() == 'X':
