#Wishva Herath for GovHack2015 Geelong


from nltk.stem.wordnet import WordNetLemmatizer
from nltk.stem import porter
import ast

from nltk.corpus import wordnet
lemmy = WordNetLemmatizer()
stemmer = porter.PorterStemmer()


class MetaTagTools(): 
    """
    Helps to generate a set of tags representing a given test.
    Uses the following modules
    nltk - for lemmatizing and stemming
    python implementation of RAKE at https://github.com/zelandiya/RAKE-tutorial
    """
    def __init__(self):
        pass

    def lowercase_all_items(self,tags):
        """
        Turns all the items of the list into lowercase

        Parameters
        ----------
        tags: a list of tags

        Returns
        -------
        lowered_tags: a list of lowercased tags
        """
        lowered_tags = []
        for tag in lowered_tags:
            lowered_tags.append(tag
    
    def correct_tag(tags):
        """
        Takes in one tag and 'corrects' it by lemmatizing it and removing entries with single values

        Parameters
        ----------
        tags: a list of tags

        """

        if type(tags) == str:
            tags = ast.literal_eval(tags)

        #break the tag if it has more than one word
        lemmy_list = []

        for single_tag in tags:
            if '>' in single_tag:
                single_tag.replace('>',' ')
            tagz = single_tag.strip().split()

            #lemmatizing
            for t in tagz:
                t = t.strip()
                if len(t) > 1:
                    try:
                        lemmy_list.append((lemmy.lemmatize(t)))
                    except:
                        pass #fix this

        #stemming
        stem_list = []
        for l in lemmy_list:
            try:
                print l
                stemmed_word = stemmer.stem(l)

                if wordnet.synsets(stemmed_word) == []:
                    #not in wordnet...revert to stemmer
                    stem_list.append(l)
                else:
                    stem_list.append(stemmer.stem(l))
            except:
                pass

        return list(set(stem_list))
