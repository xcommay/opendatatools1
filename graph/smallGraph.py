import networkx as nx
import matplotlib.pyplot as plt
import pandas as pd
tag_to_id = pd.read_csv("../metadata_dataset/data/tag_to_id_table_corrected_tags")
id_to_tag = pd.read_csv("../metadata_dataset/data/tag_table_corrected_tags.csv")


import networkx as nx
G=nx.Graph()
G.add_nodes_from(id_to_tag.id)
G.add_nodes_from(tag_to_id.tag)
import ast
for tag,id_list in zip(tag_to_id.tag, tag_to_id.id_list):
    id_list = ast.literal_eval(id_list)
    for id in id_list:
        G.add_edge(tag,id)
    
#nx.draw(G)

#plt.savefig("big.png")

nx.write_graphml(G,'so.graphml')
