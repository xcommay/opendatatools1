import pandas as pd
import urllib2
import shelve
import time
import os

os.system('wget http://data.gov.au/api/3/action/tag_list') #gets tag list from api



def readURL(shelveDB,id,url):
    """
    Accesses the url and saves the resulting text in a shelve file with the given id
    """
    try:
        file_handle = urllib2.urlopen(url)
        shelveDB[id] = file_handle.read()
    except:
        shelveDB[id + "ERROR"] = "ERROR"
        
shelveDB = shelve.open('datagovau.db') #make sure that there is not shelve file
packages = pd.read_json('package_list') #package_list is the freshly downloaded package file
prefix = "http://data.gov.au/api/3/action/package_show?id="
for row in packages.result:
    #continue #preventing from running this accidently
    url =  prefix + row
    print "working on", row
    readURL(shelveDB,str(row),str(url))
    time.sleep(1)


#converting the shelve file to text [TODO] remove the shell file altoghether takes too much space!
os.system('python shelveToText.py > datagovau.metadata.txt')


