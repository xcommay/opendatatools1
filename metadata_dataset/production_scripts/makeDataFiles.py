import sys
import pandas as pd
import ast


package_meta_file = sys.argv[1].strip()

#package_meta = pd.read_csv('../metadata_dataset/original_data/datagovau.metadata.txt',sep="\t", names=['id','json_content'])

package_meta = pd.read_csv(package_meta_file,sep="\t", names=['id','json_content']) #packagename, json string

#convert the json string into column
list_of_dicts = []
i = 0
for id,json_content in zip(package_meta.id, package_meta.json_content):
    df_json = pd.read_json(json_content)
    temp_dict = df_json.T.ix[1].to_dict()
    temp_dict['id'] = id #package name
    list_of_dicts.append(temp_dict.copy())
    del(temp_dict)
    i = i + 1

pmt = pd.DataFrame(list_of_dicts)
package_meta_table.to_csv('package_meta_table.csv')

#create tag table i,e package_id, tag_list
list_of_dicts = []
error_id_list = []

for id, tag_data in zip(pmt.id, pmt.tags):
    try:
        len(tag_data)
    except:
        error_id_list.append(id)
        continue
    if len(tag_data) == 0:
        error_id_list.append(id)
        tag_df = pd.DataFrame(tag_data)
        temp_dict= {}
        temp_dict['id'] = id
        temp_dict['tag_data'] = []
        temp_dict['tag_display_name_list'] = []
        temp_dict['tag_name_list'] = []
        temp_dict['tag_id_list'] = []
        list_of_dicts.append(temp_dict.copy())
        del(temp_dict)
        del(tag_df)
    else:
        tag_df = pd.DataFrame(tag_data)
        temp_dict= {}
        temp_dict['id'] = id
        temp_dict['tag_data'] = list(tag_data)
        temp_dict['tag_display_name_list'] = list(tag_df.display_name)
        temp_dict['tag_name_list'] = list(tag_df.name)
        temp_dict['tag_id_list'] = list(tag_df.id)
        list_of_dicts.append(temp_dict.copy())
        del(temp_dict)
        del(tag_df)

tag_table = pd.DataFrame(list_of_dicts)
tag_table.to_csv('tag_table.csv') #nice table containing tags for each id

#creating tag to id table i.e. tag, id list
from collections import defaultdict
tag_dict = defaultdict(list)
list_of_dicts = []
for id,tag_name_list in zip(tag_table.id,tag_table.tag_name_list):
    for tag in tag_name_list:
        tag_dict[tag].append(id)

list_of_dicts = []
for key in tag_dict.keys():
    temp_dict = {}
    temp_dict['tag'] = key
    temp_dict['id_list'] = tag_dict[key]
    list_of_dicts.append(temp_dict.copy())
    del(temp_dict)
    
tag_to_id_table = pd.DataFrame(list_of_dicts)
tag_to_id_table.to_csv("tag_to_id_table.csv")

tag_to_id_table['id_list_proper'] = tag_to_id_table.id_list.apply(ast.literal_eval)
tag_to_id_table['id_count'] = tag_to_id_table.id_list_proper.apply(len)

#go to part3 ipython notebook
