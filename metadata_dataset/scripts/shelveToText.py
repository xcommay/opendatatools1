import sqlite3
import pandas as pd
import shelve

s = shelve.open('datagovau.db.db')
packages = pd.read_json('package_list')

for id in packages.result:
    print id + "\t" + s[id]
